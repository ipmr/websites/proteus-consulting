<footer class="website-footer">
	<div class="container">
		<div class="mx-auto 
			px-7 
			md:px-0
			flex 
			flex-col 
			md:flex-row 
			items-center 
			justify-center 
			md:justify-between">
			<div>
				<a href="#" class="block w-32 h-auto mb-7">
					<img src="{{ asset('img/svg/logo_uas.svg') }}" class="w-full h-auto" alt="">
				</a>
			</div>
			<div>
				<nav class="text-white 
					font-bold 
					flex 
					flex-col md:flex-row items-center space-x-0 md:space-x-10 space-y-4 md:space-y-0 mb-5 uppercase">
					<a href="{{ route('inicio') }}" class="inline-block">Inicio</a>
					<a href="{{ route('nosotros') }}" class="inline-block">Nosotros</a>
					<a href="#" class="inline-block">Cursos</a>
					<a href="#" class="inline-block">Políticas</a>
					<a href="#" class="inline-block">Términos</a>
				</nav>
				<p class="text-center md:text-left text-white text-sm">
					©{{ date('Y') }} Proteus UAS - Derechos Reservados.
				</p>
			</div>
			<div class="flex flex-col text-lg text-green-400 text-center md:text-left font-bold mt-7 md:mt-0 space-y-3 md:space-y-6">
				<a href="#">info@proteusuas.com</a>
				<span>+1 (619) 739-8085</span>
			</div>
		</div>
	</div>
</footer>