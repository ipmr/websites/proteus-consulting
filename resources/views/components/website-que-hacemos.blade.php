<section class="pt-10 pb-10 md:pt-10 md:pb-24 px-7 bg-white curva-fondo">

	<x-formulario-de-registro clases="block md:hidden formulario-de-registro movil mb-20" />

	<div class="container block md:flex items-center space-x-0 md:space-x-24">
		<div class="w-full mb-7 md:mb-0 md:w-1/2">
			<p class="text-primario mb-4"><b>¿Que hacemos?</b></p>
			<h2 class="titulo mb-7">Somos pilotos e ingenieros</h2>
			<p class="descripcion">
				Proteus UAS es una empresa dedicada a las nuevas tecnologías, contando con ingenieros y pilotos de RPAS altamente capacitados para brindarle un servicio de calidad y darle solución a sus problemas.
			</p>
		</div>
		<div class="w-full md:w-1/2">
			<img src="{{ asset('/img/banners/tecnologia_e_ingenieria.jpg') }}"
			class="w-full h-auto rounded shadow-lg"
			alt="">
		</div>
	</div>
</section>