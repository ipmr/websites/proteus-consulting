<div class="card">
	<div class="flex items-center mb-7">
		<h3 class="text-lg font-bold">{{ $titulo }}</h3>
		<a href="{{ route('nueva-pagina') }}" class="btn-primary ml-auto">Nueva página</a>
	</div>
    <table class="table">
    	<thead>
    		<tr>
    			<th>Nombre</th>
    			<th>Fecha de publicacion</th>
    			<th>Editar</th>
    			<th>Eliminar</th>
    		</tr>
    	</thead>
    	<tbody>
    		<tr>
    			<td>Pagina de inicio</td>
    			<td>12 Nov 2021</td>
    			<td>
    				<a href="#" class="text-blue-400">Editar</a>
    			</td>
    			<td>
    				<a href="#" class="text-red-400">Eliminar</a>
    			</td>
    		</tr>
    	</tbody>
    </table>
</div>