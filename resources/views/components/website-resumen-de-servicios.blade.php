<section class="py-16 px-7 bg-oscuro textura-oscura">
	<div class="container mb-16">
		<div class="w-full md:w-1/2 mx-auto text-left md:text-center">
			<h2 class="titulo mb-7 text-white">
				Lorem ipsum ad his scripta blandit
			</h2>
			<p class="descripcion text-white text-opacity-75 animate__animated animate_bounce">
				Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti illo, libero pariatur soluta quo beatae repellendus iusto nesciunt consequatur, fuga atque eos aut repellat et, expedita porro repudiandae aperiam dolorum!
			</p>
		</div>
	</div>
	<div class="container block md:flex items-start space-x-0 md:space-x-5 mt-5">
		@for($i = 0; $i < 3; $i++)
			<div class="tarjeta-sin-padding w-full md:w-1/3 mb-5 md:mb-0">
				<img src="http://placehold.it/600x300/224546/ffffff" alt="">
				<div class="descripcion-de-tarjeta">
					<h2 class="subtitulo capitalize mb-5">
						Voluptate inventore reiciendis
					</h2>
					<p class="text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente impedit</p>
				</div>
			</div>
		@endfor
	</div>
	<div class="container block md:flex items-start space-x-0 md:space-x-5 mt-5">
		@for($i = 0; $i < 3; $i++)
			<div class="tarjeta-sin-padding w-full md:w-1/3 mb-5 md:mb-0">
				<img src="http://placehold.it/600x300/224546/ffffff" alt="">
				<div class="descripcion-de-tarjeta">
					<h2 class="subtitulo capitalize mb-5">
						Voluptate inventore reiciendis
					</h2>
					<p class="text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente impedit</p>
				</div>
			</div>
		@endfor
	</div>
</section>