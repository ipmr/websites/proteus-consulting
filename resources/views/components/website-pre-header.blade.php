<div class="flex items-center justify-end {{ $clases }}">
	<p class="text-sm text-white mr-5">
		<i class="fa fa-envelope mr-2"></i>
		info@proteusuas.com
	</p>
	<p class="text-sm text-white font-bold">
		<i class="fa fa-phone-alt mr-2"></i>
		+1 (619) 739-8085
	</p>
</div>