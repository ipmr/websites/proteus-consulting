<section class="py-20 bg-white px-6">
    <div class="container flex flex-col md:flex-row items-stretch space-y-6 md:space-y-0 space-x-0 md:space-x-6 mb-5">
        <x-tarjeta-de-servicio 
            titulo="Fotografía e inspección"
            icono="fotografia_e_inspeccion"
            :ruta="route('servicio', 'servicio-fotografia-e-inspeccion')">
            <x-slot name="body">
                <p>Fotografia y video 4K</p>
                <p>Avance de obra</p>
                <p>Inspección de areas de difícil acceso</p>
                <p>Termografía Radiométrica</p>
            </x-slot>
        </x-tarjeta-de-servicio>

        <x-tarjeta-de-servicio 
            titulo="Diseño y mantenimiento"
            icono="diseno_y_mantenimiento"
            :ruta="route('servicio', 'servicio-tecnologia-e-ingenieria')">
            <x-slot name="body">
                <p>Plan de mantenimiento</p>
                <p>Mantenimiento Correctivo</p>
                <p>Diseño e impresión de piezas 3D</p>
                <p>Armado de aeronaves especializadas</p>
            </x-slot>
        </x-tarjeta-de-servicio>

        <x-tarjeta-de-servicio 
            titulo="Monitoreo de riesgos"
            icono="monitoreo_de_riesgos"
            :ruta="route('servicio', 'servicio-monitoreo-de-riesgos')">
            <x-slot name="body">
                <p>Evaluación de riesgos in situ, vista aérea</p>
                <p>Monitoreo de activos de alto valor </p>
                <p>Inspección de estado de instalaciones  </p>
                <p>Emisión de recomendaciones de seguridad</p>
            </x-slot>
        </x-tarjeta-de-servicio>
    </div>

    <div class="container flex flex-col md:flex-row items-stretch space-y-6 md:space-y-0 space-x-0 md:space-x-6">
        <x-tarjeta-de-servicio 
            titulo="Capacitación"
            icono="capacitacion">
            <x-slot name="body">
                <p>Programa de capacitacion para piloto</p>
                <p>Entrenamientos especializados</p>
                <p>Diseño de casos</p>
                <p>Capacitacion en sitio o local</p>
            </x-slot>
        </x-tarjeta-de-servicio>

        <x-tarjeta-de-servicio 
            titulo="Mapeo fotogramétrico-lidar"
            icono="mapeo_fotogrametrico">
            <x-slot name="body">
                <p>Mapeo de terrenos</p>
                <p>Mediciones y generación de modelos</p>
                <p>Mediciones volumétricas</p>
            </x-slot>
        </x-tarjeta-de-servicio>

        <x-tarjeta-de-servicio 
            titulo="Consultoría"
            icono="consultoria"
            :ruta="route('servicio', 'servicio-consultoria')">
            <x-slot name="body">
                <p>Implementation de programa con UAS</p>
                <p>Pruebas de concepto</p>
                <p>Evaluación de equipo para sus necesidades</p>
            </x-slot>
        </x-tarjeta-de-servicio>
    </div>
</section>