<form action="{{ route('registro-de-usuario') }}" class="{{ $clases }}" method="post">
	@if (session('mensaje_de_registro'))
		<div>
			<h1 class="titulo-formulario text-center font-bold text-green-600">
				{{ session('registro.nombre') }}
			</h1>
			<p class="text-gray-500 mb-7 leading-relaxed text-center">
				{{ session('mensaje_de_registro') }}
			</p>
		</div>
	@else
		<div class="w-full">
			@csrf
			@if($errors->any())
				<h1 class="titulo-formulario text-center font-bold text-red-600">
				Algo salió mal!
				</h1>
			@else
				<h1 class="titulo-formulario text-center font-bold text-primario">
					Mantente en contacto
				</h1>
				<p class="text-gray-500 mb-7 leading-relaxed text-center">
					Regístrate para que no te pierdas nuestras actividades, cursos y eventos
				</p>
			@endif
			<div class="mb-4">
				<label for="">Nombre completo:</label>
				<input type="text" value="{{ old('nombre') }}" name="nombre" class="campo-texto" required>
				@error('nombre')
				<span class="text-red-500 text-sm">{{ $message }}</span>
				@enderror
			</div>
			<div class="mb-4">
				<label for="">Correo electrónico:</label>
				<input type="email" value="{{ old('email') }}" name="email" class="campo-texto" required>
				@error('email')
				<span class="text-red-500 text-sm">{{ $message }}</span>
				@enderror
			</div>
			<div class="mb-7">
				<label for="">Compañia (opcional):</label>
				<input type="text" value="{{ old('compania') }}" name="compania" class="campo-texto">
				@error('compania')
				<span class="text-red-500 text-sm">{{ $message }}</span>
				@enderror
			</div>
			<button class="btn btn-envio">
			<i class="fa fa-check fa-xs mr-2"></i>
			Regístrate
			</button>
		</div>
	@endif
</form>