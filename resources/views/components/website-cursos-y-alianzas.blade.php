<div>
	<section class="py-10 md:py-24 px-7 bg-oscuro textura-oscura">
		<div class="container block md:flex items-center space-x-0 md:space-x-24">
			<div class="w-full md:w-1/2 mb-7 md:mb-0">
				<h1 class="titulo text-white mb-7">Cursos</h1>
				<p class="descripcion text-green-300">
					Brindamos cursos para la obtención de licencia de piloto de RPAS ante la AFAC y cursos de adiestramiento para diferentes aplicaciones.
				</p>
			</div>

			<div class="w-full md:w-1/2">
				<h1 class="titulo text-white mb-7">Alianza</h1>
				<p class="descripcion text-green-300">
					Contamos con alianzas clave en el sector como el Clúster Aeroespacial de Baja California, el instituto Tecnológico de Tijuana y la Universidad Autónoma de Baja California. 
				</p>
			</div>
		</div>
	</section>

	<section class="py-10 md:py-10 px-20 md:px-20 bg-white">
		<div class="container flex flex-wrap md:flex-nowrap items-center space-y-5 md:space-y-0 space-x-0 md:space-x-36">
			<div class="w-full md:w-1/4 flex items-center justify-center">
				<img class="h-32 w-auto" src="{{ asset('img/logos/agencia_federal_de_aviacion_civil.jpg') }}" alt="">
			</div>
			<div class="w-full md:w-1/4 flex items-center justify-center">
				<img src="{{ asset('img/logos/baja_aerospace_cluster.jpg') }}" alt="">
			</div>
			<div class="w-full md:w-1/4 flex items-center justify-center">
				<img class="h-32 w-auto" src="{{ asset('img/logos/itt.jpg') }}" alt="">
			</div>
			<div class="w-full md:w-1/4 flex items-center justify-center">
				<img class="h-32 w-auto" src="{{ asset('img/logos/uabc.jpg') }}" alt="">
			</div>
		</div>
	</section>
</div>