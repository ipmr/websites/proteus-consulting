<a href="{{$ruta}}" class="block p-7 rounded-md text-center shadow-md flex-1 {{ $bgcolor }} @if($transparency == true) bg-opacity-50 @endif">
    @if($icono)
    <div class="flex items-center justify-center mb-10">
        <img src="/img/iconos/{{ $icono }}.png" class="h-20 w-auto" alt="">
    </div>
    @endif
    <h3 class="font-bold mb-6 uppercase">{{ $titulo }}</h3>
    <div class="text-sm">
        {{ $body }}
    </div>
</a>