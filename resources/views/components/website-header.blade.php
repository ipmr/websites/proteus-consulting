<div class="website-header-container">
	<header class="website-header px-7">
		<div class="container flex items-center justify-center">
			<a href="{{ route('inicio') }}" class="ml-0">
				<img src="{{ asset('img/svg/logo_uas.svg') }}" class="logo" alt="">
			</a>
			<x-website-nav clases="ml-auto" />
		</div>
	</header>
</div>