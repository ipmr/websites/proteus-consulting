<section class="website-portada relative">
	<banners-de-portada></banners-de-portada>
	<x-formulario-de-registro clases="hidden md:flex formulario-de-registro" />
	<img src="{{ asset('img/svg/curva_portada.svg') }}" class="curva-portada" alt="">
</section>