<div class="bg-oscuro textura-oscura pt-48 pb-10">
    <div class="container text-center">
    	<div class="w-full md:w-2/3 mx-auto px-4">
    		<h1 class="text-white text-4xl uppercase font-bold">{{ $titulo }}</h1>
    		@isset($subtitulo)
    		<p class="mt-7 text-green-300 text-lg">{!! $subtitulo !!}</p>
    		@endisset
            {{ $extra }}
    	</div>
    </div>
</div>