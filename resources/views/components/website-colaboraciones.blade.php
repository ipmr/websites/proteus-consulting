<section class="py-10 md:py-24 px-7 bg-negro relative z-10 shadow">

		<div class="container flex flex-col md:flex-row space-x-0 md:space-x-24 space-y-7 md:space-y-0 items-center">
			<div class="w-full md:w-2/3 text-center md:text-left">
				<h2 class="titulo mb-7 text-white">Colaboraciones</h2>
				<p class="descripcion text-green-300 text-opacity-50 mb-7">
					Hemos colaborado con empresas públicas y privadas entre las que se encuentran:
				</p>
			</div>
			<div class="w-full md:w-1/3 text-center md:text-left flex flex-col space-y-6">
				<div class="card p-8 bg-white">
					<img src="{{ asset('img/logos/LOGO_PROTEUS_CONSULTING.png') }}" alt="">
				</div>
				<div class="card p-8 bg-white">
					<img src="{{ asset('img/logos/DYNACOM_LOGO.png') }}" alt="">
				</div>
			</div>
		</div>

		<!-- <div class="container mt-10 text-left md:text-center text-white"> -->
			<!-- <div class="w-2/3 mx-auto flex items-center space-x-20">
				<img src="http://placehold.it/500x300/224546/ffffff" class="w-full shadow rounded" alt="">
				<img src="http://placehold.it/500x300/224546/ffffff" class="w-full shadow rounded" alt="">
				<img src="http://placehold.it/500x300/224546/ffffff" class="w-full shadow rounded" alt="">
			</div> -->
		<!-- </div> -->
</section>