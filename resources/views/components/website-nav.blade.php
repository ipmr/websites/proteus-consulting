<div class="{{ $clases }}">
	<x-website-pre-header clases="hidden md:flex"></x-website-pre-header>
	<div class="flex ml-auto mt-4">
		<website-nav-links clases="hidden md:block website-nav"></website-nav-links>
		<mobile-nav-btn clases="block md:hidden ml-auto" />
	</div>
</div>