@component('mail::message')
# Hola {{ $nombre }}

Gracias por contactarnos en Proteus UAS, te notificamos que recibimos tu mensaje, te responderemos a la brevedad posible.

{{ config('app.name') }}
@endcomponent
