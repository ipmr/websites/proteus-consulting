@component('mail::message')
# Hola Administrador, el C. {{ $nombre }}

ha generado una suscripción en Proteus UAS.

{{ config('app.name') }}
@endcomponent
