@component('mail::message')
# Nuevo mensaje recibido de {{ $nombre }}
Email: {{$email}}
<br>
Asunto: {{$asunto}}
<br>
Mensaje: {{$mensaje}}
<br>
@isset($interes)
Estoy interesado en:  {{ucwords(str_replace("-"," ",$interes))}}
@endisset
{{ config('app.name') }}
@endcomponent
