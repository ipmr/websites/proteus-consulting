<x-app-layout>
	<x-slot name="header">
	    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
	        {{ __('Bienvenido') }}
	    </h2>
	</x-slot>

	<x-tabla-de-paginas titulo="Páginas creadas recientemente" />

</x-app-layout>