<x-app-layout>
	<x-slot name="header">
	    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
	        {{ __('Nueva Página') }}
	    </h2>
	</x-slot>

	<div class="card">
		<formulario-pagina></formulario-pagina>
	</div>

</x-app-layout>