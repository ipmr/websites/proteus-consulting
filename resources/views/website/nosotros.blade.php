<x-guest-layout>
	<x-website-header />
	<x-website-titulo-de-pagina 
		titulo="Acerca de Proteus UAS" 
		subtitulo="Proteus Consulting UAS es una empresa que se fundo en Tijuana B.C. en el año 2019, como una rama de la empresa de seguridad <a href='https://proteusconsulting.com' class='underline text-yellow-300' target='_blank'>Proteus Consulting</a>" />
	<x-website-que-hacemos />
	<section class="px-4 py-10 md:py-20">
		<div class="container">
			<div class="w-full md:w-1/2 mx-auto text-center">
				<p class="text-lg text-gray-700 mb-20">
					Somos un proyecto de integración tecnológica en vehículos aéreos tripulados y no tripulados para ofrecer distintos tipos de servicios, aplicaciones y productos, apegados siempre a las normas y regulaciones aplicables. 
				</p>
				<div class="flex flex-col md:flex-row items-center space-x-0 md:space-x-20 space-y-10 md:space-y-0">
					<div class="flex-1 text-center md:text-left">
						<h1 class="text-3xl font-bold uppercase mb-7">Misión</h1>
						<p>
							Ser los principales proveedores de servicios en el sector de vehículos aéreos no tripulados con soluciones tecnológicas e innovadoras en México y USA
						</p>
					</div>
					<div class="flex flex-col space-y-3 flex-1 text-center md:text-left">
						<h3 class="font-bold uppercase flex items-center justify-center md:justify-start">
							<i class="fa fa-fw fa-lg fa-users-cog mr-3 text-green-600"></i>
							<span>Responsabilidad</span>
						</h3>
						<h3 class="font-bold uppercase flex items-center justify-center md:justify-start">
							<i class="fa fa-fw fa-lg fa-user-plus mr-3 text-green-600"></i>
							<span>Confiabilidad</span>
						</h3>
						<h3 class="font-bold uppercase flex items-center justify-center md:justify-start">
							<i class="fa fa-fw fa-lg fa-chart-line mr-3 text-green-600"></i>
							<span>Competitividad</span>
						</h3>
						<h3 class="font-bold uppercase flex items-center justify-center md:justify-start">
							<i class="fa fa-fw fa-lg fa-lightbulb mr-3 text-green-600"></i>
							<span>Innovación</span>
						</h3>
						<h3 class="font-bold uppercase flex items-center justify-center md:justify-start">
							<i class="fa fa-fw fa-lg fa-handshake mr-3 text-green-600"></i>
							<span>Integridad</span>
						</h3>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="py-10 md:py-20 px-4 bg-negro">
		<div class="container">
			<div class="w-full md:w-2/3 flex flex-col md:flex-row items-center mx-auto space-x-0 md:space-x-10 space-y-10 md:space-y-0">
				<div class="flex-1 text-center md:text-left">
					<h3 class="text-white font-bold text-3xl uppercase mb-7">Nuestro Equipo</h3>
					<p class="text-white text-green-300">
						Somos una empresa con mucho potencial para cubrir diferentes áreas del medio, con un equipo de ingenieros aeronáuticos y aeroespaciales que proveen soluciones tecnológicas para las diferentes necesidades de nuestros clientes 
					</p>
				</div>
				<div class="flex-1">
					<img src="{{ asset('img/NuestroEquipo.png') }}" class="w-full shadow-lg h-auto" alt="">
				</div>
			</div>
		</div>
	</section>
	<section class="py-10 md:py-20 px-4 bg-white">
		<div class="container flex flex-col md:flex-row items-center space-x-0 md:space-x-24 space-y-10 md:space-y-0">
			<div class="w-full md:w-2/3 text-center md:text-left">
				<h1 class="text-3xl font-bold uppercase mb-7">¿Porque Elegirnos?</h1>
				<p class="mb-4">
					Somos la primera empresa de servicios de drones en el Noroeste de México con grandes capacidades de resolver las necesidades de nuestros clientes adaptando las nuevas tecnologías. Con el objetivo único que nuestros clientes tengan éxito.
				</p>
				<p>
					 Desde la definición de la misión hasta la selección de productos, desde el inicio del programa hasta la conclusión ofrecemos soluciones de alta calidad con ingenieros y expertos para satisfacer y superar las expectativas de nuestros clientes.
				</p>
			</div>
			<div class="w-1/3 flex flex-col space-y-4 text-center md:text-left">
				<h3 class="text-lg font-bold uppercase">Pasión y Profesión</h3>
				<h3 class="text-lg font-bold uppercase">Calidad</h3>
				<h3 class="text-lg font-bold uppercase">Soluciones Eficientes</h3>
			</div>
		</div>
	</section>
</x-guest-layout>