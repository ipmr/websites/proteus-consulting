<x-guest-layout>
<x-website-header />
<x-website-titulo-de-pagina 
        titulo="Contactanos" 
        subtitulo="Envíanos un mensaje y resolveremos cualquier duda o pregunta lo antes posible." />
        <div class="container mt-8 ">
            <div class="w-full md:w-1/2 mx-auto text-center">
                <div class="flex flex-col md:flex-row items-center space-x-0 md:space-x-20 space-y-10 md:space-y-0">
                    <div class="flex-1 text-center md:text-left">
                        <p class="text-2x1 font-bold text-center uppercase">
                            Tel. +1 (619) 739-8085
                        </p>
                    </div>
                </div>
            </div>
        </div>
        @if (session('mesaje_de_contacto'))
        <div>
            <h1 class="titulo-formulario text-center font-bold text-green-600">
                {{ session('mensaje.nombre') }}
            </h1>
            <p class="text-gray-500 mb-7 leading-relaxed text-center">
                {{ session('mesaje_de_contacto') }}
            </p>
        </div>
        @else
        <div class="container px-7 my-10">
            <div class="w-full md:w-1/3 mx-auto">
                <form action="{{ route('nuevo-contacto') }}" method="post">
                    @csrf
                    @if($errors->any())
                    <h1 class="titulo-formulario text-center font-bold text-red-600">
                        Algo salió mal!
                    </h1>
                    @else
                    <h1 class="titulo-formulario text-center font-bold text-primario">
                    Mantente en contacto
                    </h1>
                    @endif
                    <div class="mb-6">
                        <label for="">Nombre completo:</label>
                        <input type="text" class="campo-texto" placeholder="Nombre Completo" name="nombre" required>
                        @error('nombre')
                        <span class="text-red-500 text-sm">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-6">
                        <label for="">Email:</label>
                        <input type="email" name="email" placeholder="usuario@dominio.com" class="campo-texto" required>
                        @error('email')
                        <span class="text-red-500 text-sm">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-6">
                        <label for="">Asunto:</label>
                        <input type="text" name="asunto" placeholder="¿En que podemos ayudarte?" class="campo-texto" required>
                        @error('asunto')
                        <span class="text-red-500 text-sm">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-6">
                        @if(request()->has('int'))
                            <label for=int>¿Estas interesado en algun curso en particular?</label>
                            <select class="campo-texto" name="interes" id="interes">}
                                <option disabled selected>Selecciona algun curso.</option>
                                <option @if(request()->int == 'curso-de-piloto') selected @endif value="curso-de-piloto">
                                    Curso de piloto.
                                </option>
                                <option @if(request()->int == 'curso-uso-recreativo') selected @endif value="curso-uso-recreativo">
                                Curso uso recreativo.
                                </option>
                                <option @if(request()->int == 'curso-seguridad-publica') selected @endif value="curso-seguridad-pública" >
                                Curso de seguridad pública.
                                </option>
                                <option @if(request()->int == 'curso-de-seguridad-privada') selected @endif value="curso-de-seguridad-privada"> 
                                Curso de seguridad privada.
                                </option>
                                <option @if(request()->int == 'curso-de-especializacion') selected @endif value="curso-de-especializacion">
                                Curso de especialización
                                </option>
                                <option @if(request()->int == 'servicio-consultoria') selected @endif value="servicio-de-consultoria">
                                Consultoría.
                                </option>
                                <option @if(request()->int == 'fotografia-e-inspeccion') selected @endif value="fotografia-e-inspeccion">
                                Fotografía e Inspección.
                                </option>
                                <option @if(request()->int == 'segirodad-dfr') selected @endif value="segirodad-dfr">
                                Segurdiad DFR.
                                </option>
                                <option @if(request()->int == 'servicio-tecnologia-e-ingenieria') selected @endif value="servicio-tecnologia-e-ingenieria" >
                                Tecnología e ingeniería para aeronaves.
                                </option>
                                <option value="otro">Otros</option>
                            </select>
                        @endif
                        @error('interes')
                            <span class="text-red-500 text-sm">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-6">
                        <label for="mensaje"> Mensaje</label>
                        <input type="textarea" name="mensaje" placeholder="Cuentanos mas sobre tus dudas." class="campo-texto" required>
                        @error('mensaje')
                        <span class="text-red-500 text-sm">{{ $message }}</span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-envio btn-block">Enviar</button>
                </form>
            </div>
        </div>
        @endif
</x-guest-layout>