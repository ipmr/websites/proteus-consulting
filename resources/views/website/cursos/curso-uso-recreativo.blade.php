<x-guest-layout>
<x-website-titulo-de-pagina
titulo="Capacitación para uso recreativo"
subtitulo="En este curso se aborda la teoría y práctica necesarias para volar tu drone de forma recreativa"/>
<section class="py-14 px-4">
	<div class="container flex flex-col md:flex-row items-center space-x-0 md:space-x-14 space-y-10 md:space-y-0">
		<div class="mx-auto w-full md:w-1/2">
			<h3 class="text-2xl font-bold mb-4">Fórmate para introducirte en el futuro de la recreación</h3>
			<p>Este curso va dirigido para todas las personas que deseen aprender a pilotar y conocer un RPAS de forma correcta para uso recreativo. No es necesario tener conocimientos previos o tener tu propio Drone.</p>
		</div>
		<div class="w-full md:w-1/2">
			<img src="{{ asset('img/cursos/curso_uso_recreativo.jpg') }}" class="shadow rounded w-full" alt="">
		</div>
	</div>
</section>

<section class="py-14 px-4 bg-oscuro">
	<div class="container">
		<h3 class="text-lg font-bold text-white mb-7">
        El curso de RPAS para uso recreativo está basado en los requisitos de la NOM-107-SCT3-19.
		</h3>
		<div class="flex flex-col md:flex-row items-start space-x-0 md:space-x-10 space-y-10 md:space-y-0">
        <H3 class="text-lg font-bold text-white mb-7"> 
        El curso de piloto de RPAS para uso recreativo se imparte siguiendo un esquema de 
        aprendizaje que le permitirá al alumno comprender todo lo relacionado a las Aeronaves 
        Pilotadas a Distancia RPA 
        </H3>
		</div>
	</div>
</section>


<section class="py-14 px-4 bg-green-100">
	<div class="container">
		<h3 class="text-lg font-bold mb-7">Temario de curso</h3>
		<div class="flex flex-col md:flex-row items-start space-y-10 md:space-y-0">
			<div class="w-full md:w-1/2">
				<h3 class="font-bold mb-7">CONTENIDO MÓDULO I</h3>
				<div class="flex flex-col space-y-3 font-sm">
					<p>I.-Bienvenida e introducción a las instalaciones y curso.</p>
					<p>II.-Introducción al medio aeronáutico</p>
					<p>III.-Aerodinámica de RPAS</p>
					<p>IV.-Sistemas gnerales de RPAS</p>
					<p>V.-Procedimientos operacionales (manual de operaciones)</p>
					<p>VI.-Meteorología</p>
					<p>VII.-Rendimiento de la aeronave(Performance)</p>
					<p>VIII.-Peso y balance en los tipos de RPAS</p>
					<p>IX.-Factores humanos para RPAS</p>
					<p>X.-Seguridad aérea y gestión de riesgos (SMS)</p>
				</div>
			</div>
			<div class="w-full md:w-1/2">
				<h3 class="font-bold mb-7">CONTENIDO TEMÁTICO MÓDULO II</h3>
				<div class="flex flex-col space-y-3 font-sm">
					<p>I.-Requerimientos normativos de RPAS</p>
					<p>II.-Familiarización con RPAS (Hardware/Software/Firmware)</p>
					<p>III.-Procedimientos de operación y modos de vuelo</p>
					<p>IV.-Planeación de un vuelo de RPAS (Inspección pre vuelo)</p>
					<p>V.-Información y seguridad de las baterías</p>
					<p>VI.-Control remoto y sincronización</p>
					<p>VII.-Practicas de simulador</p>
					<p>VIII.-Practicas de vuelo</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="py-14 px-4 bg-oscuro">
<div class="container">
<div class="grid grid-cols-1 md:grid-cols-1">
<img src="{{ asset('img/cursos/recreativo_1.jpg') }}" alt="Recretivo_1">
<img src="{{ asset('img/cursos/recreativo_3.jpg') }}" alt="Recreativo_3">
<img src="{{ asset('img/cursos/recreativo_2.jpg') }}" alt="Recreativo_2">

</div>
</div>
<br>
		<center>	
		<a href="/contacto?int=curso-uso-recreativo" class="btn btn-envio">Obtener Cotización, Contactanos dando click aqui.</a>
		</center>
		<br>
</section>


</x-guest-layout>
