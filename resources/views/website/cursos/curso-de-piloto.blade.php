<x-guest-layout>
<x-website-titulo-de-pagina
titulo="Certificate como piloto de RPAS pequeño"
subtitulo="En este curso se aborda la teoría y practica necesarias para obtener tu licencia de RPAS ante AFAC" />
<section class="py-14 px-4">
	<div class="container flex flex-col md:flex-row items-center space-x-0 md:space-x-14 space-y-10 md:space-y-0">
		<div class="mx-auto w-full md:w-1/2">
			<h3 class="text-2xl font-bold mb-4">Fórmate para introducirte en una industria llena de oportunidades</h3>
			<p>La industria de los RPAS (Drones/UAS) en los últimos años ha crecido de manera exponencial, que debido a sus distintos usos en la industria ha generado cada vez más demanda de pilotos para llevar a cabo estas operaciones</p>
		</div>
		<div class="w-full md:w-1/2">
			<img src="{{ asset('img/cursos/curso_de_piloto.jpg') }}" class="shadow rounded w-full" alt="">
		</div>
	</div>
</section>
<section class="py-14 px-4 bg-oscuro">
	<div class="container">
		<div class="flex flex-col md:flex-row items-start space-x-0 md:space-x-14 space-y-10 md:space-y-0 mx-auto">
			<div class="w-full md:w-1/2">
				<h3 class="font-bold text-lg mb-7 text-white">¿Por qué debería elegirnos?</h3>
				<div class="flex flex-col space-y-3 text-green-300">
					<p class="text-sm">
						1.Experiencia en operaciones
					</p>
					<p class="text-sm">
						2.Capacitadores certificados, pilotos e ingenieros
					</p>
					<p class="text-sm">
						3.Materiales de alto valor
					</p>
					<p class="text-sm">
						4.Confianza. Con referencia de agencias gubernamentales y privadas
					</p>
					<p class="text-sm">
						5.Curso completo y estructurado según la reglamentación
					</p>
					<p class="text-sm">
						6.Clases reducidas. Con alto soporte para nuestros alumnos
					</p>
					<p class="text-sm">
						7.Cursos Homologados por AFAC
					</p>
				</div>
			</div>
			<div class="w-full md:w-1/2">
				<p class="mb-7 text-lg font-bold text-white">
					La capacitación significativa, práctica y eficientees una de las cosas más importantes que podemos hacer por ti
				</p>
				<p class="text-white">
					La Association for Unmanned Vehicle Systems International(AUVSI) afirma que "en los primeros tres años de integración de los drones se crearán más de 70.000 puestos de trabajo solo en los Estados Unidos con un impacto económico de más de 13.600 millones de dólares. Este beneficio crecerá hasta el año 2025, en el que prevemos la creación de más de 100.000 puestos de trabajo y un impacto económico de 82.000 millones de dólares”.
				</p>
			</div>
		</div>
	</div>
</section>
<section class="py-14 px-4">
	<div class="container">
		<h3 class="text-lg font-bold mb-7">
		La Secretaria de Comunicaciones y Transportes clasifica a las operaciones de RPAS en tres tipo
		</h3>
		<div class="flex flex-col md:flex-row items-start space-x-0 md:space-x-10 space-y-10 md:space-y-0">
			<div class="w-full md:w-1/2">
				<p class="font-bold text-lg text-green-600 mb-7">Necesitan licencia</p>
				<div class="flex flex-col space-y-3">
					<p>
						1. RPAS de uso Comercial. Destinado por el operador de RPAS a realizar tareas con fines de lucro.
					</p>
					<p>
						2. RPAS de uso Privado No Comercial. Destinado por el operador de RPAS a realizar tareas sin fines de lucro
					</p>
				</div>
			</div>
			<div class="w-full md:w-1/2">
				<p class="font-bold text-lg text-green-600 mb-7">No Necesitan licencia</p>
				<div class="flex flex-col space-y-3">
					<p>
						3. RPAS  de  uso  Recreativo. Destinado por el  operador de RPAS a la recreación
					</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="py-14 px-4 bg-green-100">
	<div class="container">
		<h3 class="text-lg font-bold mb-7">Temario de curso</h3>
		<div class="flex flex-col md:flex-row items-start space-y-10 md:space-y-0">
			<div class="w-full md:w-1/2">
				<h3 class="font-bold mb-7">CONTENIDO MÓDULO I</h3>
				<div class="flex flex-col space-y-3 font-sm">
					<p>I.-Introducción al medio aeronáutico</p>
					<p>II.-Reglamentación aérea</p>
					<p>III.-Servicios de tránsito aéreo</p>
					<p>IV.-Aerodinámica de RPAS</p>
					<p>V.-Sistemas generales de RPAS</p>
					<p>VI.-Procedimientos operacionales (Manual de operaciones)</p>
					<p>VII.-Navegación, interpretación de mapas</p>
					<p>VIII.-Comunicación y fraseología</p>
					<p>IX.-Meteorología</p>
					<p>X.-Rendimientos de la aeronave (Performance)</p>
					<p>XI.-Peso y balance en los tipos de RPAS</p>
					<p>XII.-Aeronavegabilidad de RPAS</p>
					<p>XIII.-Factores humanos para RPAS</p>
					<p>XIV.-Seguridad aérea y gestión de riesgos (SMS)</p>
				</div>
			</div>
			<div class="w-full md:w-1/2">
				<h3 class="font-bold mb-7">CONTENIDO TEMÁTICO MÓDULO</h3>
				<div class="flex flex-col space-y-3 font-sm">
					<p>I.-Requerimientos normativos de RPAS</p>
					<p>II.-Familiarización con RPAS (Hardware/Software/Firmware)</p>
					<p>III.-Procedimientos de operación y modos de vuelo</p>
					<p>IV.-Planeación de un vuelo de RPAS (Inspección pre vuelo)</p>
					<p>V.-Información y seguridad de las baterías</p>
					<p>VI.-Control remoto y sincronización</p>
					<p>VII.-Practicas de simulador</p>
					<p>VIII.-Practicas de vuelo</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="py-14 px-4">
	<div class="container">
		<div class="w-full md:w-2/3 mx-auto">
			<h3 class="font-bold text-lg mb-7">¿Para que puedes usar tu licencia de RPAS?</h3>
			<p>Un RPAS (Drone/UAS)es una herramienta poderosa y práctica para muchas industrias. El sector inmobiliario es salida laboral más para un piloto de drones con licencia; hay una necesidad constante de video e imágenes aéreas fijas de bienes inmuebles. Sin embargo, los drones se están utilizando en un número creciente de industrias en todo el mundo</p>
			<br>
			<center>	
			<a href="/contacto?int=curso-de-piloto" class="btn btn-envio">Obtener Cotización, Contactanos dando click aqui.</a>
			</center>
			<br>
		</div>
	</div>
</section>
</x-guest-layout>