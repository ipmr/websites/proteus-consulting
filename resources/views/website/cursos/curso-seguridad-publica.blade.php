<x-guest-layout>
<x-website-titulo-de-pagina
titulo="Capacitación para la seguridad publica"
subtitulo="En este curso se aborda la teoría y práctica necesarias para implementar el uso de drones en la seguridad pública de una manera eficiente. "/>

<section class="py-14 px-4">
	<div class="container flex flex-col md:flex-row items-center space-x-0 md:space-x-14 space-y-10 md:space-y-0">
		<div class="mx-auto w-full md:w-1/2">
			<h3 class="text-2xl font-bold mb-4">Aplique la innovación a su programa de seguridad</h3>
			<p>La aplicación de RPAS para la seguridad garantiza un trabajo inteligente, 
            escalable, asequible, accesible y más simple que nunca. Proporciona soluciones 
            de vigilancia y monitoreo con vídeo en directo sin ataduras físicas, aumente la 
            seguridad general y la eficiencia operativa que entrega una vista aérea. </p>
		</div>
		<div class="w-full md:w-1/2">
			<img src="{{ asset('img/cursos/seguridad_publica.jpg') }}" class="shadow rounded w-full" alt="">
		</div>
	</div>
</section>

<section class="py-14 px-4 bg-oscuro">
	<div class="container">
		<div class="flex flex-col md:flex-row items-start space-x-0 md:space-x-14 space-y-10 md:space-y-0 mx-auto">
			<div class="w-full md:w-1/2">
				<h3 class="font-bold text-lg mb-7 text-white">Aplicaciones Potenciales </h3>
				<div class="flex flex-col space-y-3 text-green-300">
					<p class="text-sm">
                    • Respuesta para emergencias
					<p class="text-sm">
                    • Monitoreo bajo demanda
					</p>
					<p class="text-sm">
                    • Control de masas
					</p>
					<p class="text-sm">
                    • Video en tiempo real
					</p>
					<p class="text-sm">
                    • Patrullaje perimetral
					</p>
					<p class="text-sm">
                    • Seguimiento y monitoreo de riesgos
					</p>
					<p class="text-sm">
                    • Evaluación de amenazas
                    </p>
					</p>
				</div>
			</div>
			<div class="w-full md:w-1/2">
				<p class="mb-7 text-lg font-bold text-white">
                Aplique la innovación a su programa de seguridad
				</p>
				<p class="text-white">Los RPAS permiten cubrir eficientemente grandes áreas en sus operaciones con la capacidad de cubrir 
                rápidamente esas áreas y utilizar cargas especializadas para las distintas necesidades requeridas en la 
                vigilancia y monitoreo como ejemplo de ello son las cámaras térmicas FLIR, megáfonos, luces, sensores.				</p>
			</div>
		</div>
		<br>
		<center>	
		<a href="/contacto?int=curso-seguridad-publica" class="btn btn-envio">Obtener Cotización, Contactanos dando click aqui.</a>
		</center>
		<br>
	</div>
</section>


</x-guest-layout>
