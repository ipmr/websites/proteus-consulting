<x-guest-layout>
<x-website-titulo-de-pagina
titulo="Curso de Seguridad Privada"
subtitulo="En este curso se aborda la teoría y práctica necesarias para implementar el uso de drones en la seguridad privada de una manera eficiente." />
<section class="py-14 px-4">
	<div class="container flex flex-col md:flex-row items-center space-x-0 md:space-x-14 space-y-10 md:space-y-0">
		<div class="mx-auto w-full md:w-1/2">
			<h3 class="text-2xl font-bold mb-7">Aplique la innovación a su programa de seguridad</h3>
			
			<p>
				La  aplicación  de  RPAS  para  la  seguridad  garantiza  un  trabajo  inteligente, escalable, asequible, accesible y más simple que nunca. Proporciona solucionesde vigilancia y monitoreo con vídeo en directo sin ataduras físicas, aumente la seguridad general y la eficiencia operativa que entregauna vista aérea.
			</p>
		</div>
		<div class="w-full md:w-1/2">
			<img src="{{ asset('img/cursos/seguridad_privada.jpg') }}" class="shadow rounded w-full" alt="">
		</div>
	</div>
</section>

<section class="py-14 px-4 bg-oscuro">
	<div class="container flex items-center flex-col md:flex-row space-x-0 md:space-x-14 space-y-10 md:space-y-0">
		<div class="w-full md:w-1/2">
			<h3 class="text-lg font-bold mb-7 text-white">Aplicaciones Potenciales </h3>
			<div class="flex flex-col space-y-3 text-green-300">
				<p>Respuesta para emergencias</p>
				<p>Monitoreo bajo demanda</p>
				<p>Conciencia situacional de las instalaciones</p>
				<p>Video en tiempo real</p>
				<p>Patrullaje perimetral rutinaria</p>
				<p>Seguimiento y monitoreo de riesgos</p>
				<p>Evaluación de amenazas</p>
			</div>
		</div>
		<div class="w-full md:w-1/2">
			<p class="text-lg text-white">
				Los RPAS permiten cubrir eficientemente grandes áreas en su búsqueda de individuos, pero los RPAS son capaces de cubrir rápidamente esas áreas y pueden utilizar cargas especializadas para las distintas necesidades requeridas en la vigilancia y monitoreo como ejemplo de ello son las cámaras térmicas FLIR
			</p>
		</div>
	</div>
	<br>
	<center>	
	<a href="/contacto?int=curso-de-seguridad-privada" class="btn btn-envio">Obtener Cotización, Contactanos dando click aqui.</a>
	</center>
	<br>
</section>

</x-guest-layout>