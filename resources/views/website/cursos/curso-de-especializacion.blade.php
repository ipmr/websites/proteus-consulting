<x-guest-layout>
<x-website-titulo-de-pagina
titulo="Capacitación para aplicaciones específicas"
subtitulo="Si  tienes  los  conocimientos  básicos  teóricos  y  prácticos  para  volar  un  RPA  y  te  interesa  aprender  alguna aplicación para emprender tú negocio o trabajo o simplemente quieres aprender, nosotros podemos ayudarte." />
<section class="py-14 px-4">
	<div class="container flex flex-col md:flex-row items-center space-x-0 md:space-x-14 space-y-10 md:space-y-0">
		<div class="mx-auto w-full md:w-1/2">
			<h3 class="text-2xl font-bold mb-7">Proteus UAS te ofrece una amplia gama de cursos de especialización prácticos y teóricos, con el equipo y lugar adecuado</h3>
			
			<h4 class="text-lg font-bold mb-4">Aplicaciones Potenciales</h4>

			<div class="flex flex-col space-y-2">
				<p>Fotografía Aérea </p>
				<p>Cinematografía Aérea </p>
				<p>Fotogrametría </p>
				<p>Agricultura </p>
				<p>Monitoreo para construcción </p>
				<p>Inspecciones generales</p>
				<p>Evaluación de amenazas</p>
			</div>
		</div>
		<div class="w-full md:w-1/2">
			<img src="{{ asset('img/cursos/curso_especializacion.jpg') }}" class="shadow rounded w-full" alt="">
		</div>
	</div>
</section>

<section class="py-14 px-4 bg-oscuro">
	<div class="container">
		<img src="{{ asset('img/cursos/curso_especializacion_grid.png') }}" class="w-full h-auto" alt="">
	</div>
	<br>
	<center>	
	<a href="/contacto?int=curso-de-especializacion" class="btn btn-envio">Obtener Cotización, Contactanos dando click aqui.</a>
	</center>
	<br>
</section>

</x-guest-layout>