<x-guest-layout>
<x-website-titulo-de-pagina
titulo="Monitoreo de riesgos con UAS-DRONES"
subtitulo="Reduce los riesgos e incrementa la resiliencia de la operación">
	<x-slot name="extra">
			<p class="mt-5 text-white">
				Generamos valor para nuestros clientes, con el monitoreo periódico de sus activos desde una perspectiva aérea, generando
				recomendaciones y reportes ejecutivos, con el objetivo de ayudar a las partes interesadas a la toma de acciones para la mejora
				en su seguridad.
		    </p>
	</x-slot>
</x-website-titulo-de-pagina>

<section class="py-14 px-4 bg-white">
	<div class="w-8/12 mx-auto flex flex-col md:flex-row items-center space-x-0 md:space-x-14 space-y-10 md:space-y-0">
		<div class="w-full md:w-1/3">
			<img src="/img/iconos/proceso_monitoreo.png" class="w-full h-auto" alt="">
		</div>
		<div class="mx-auto w-full md:w-2/3">
			<p class="font-bold text-lg mb-7">Obtenga una fuente continua de información de valor para sus instalaciones e infraestructura exterior critica.</p>
		</div>
	</div>
</section>

<section class="bg-green-100 py-14 px-7">
	<div class="container flex flex-col md:flex-row items-stretch space-y-6 md:space-y-0 space-x-0 md:space-x-6 mb-5">
	    <x-tarjeta-de-servicio 
	        titulo="Identificación"
	        icono="identificacion"
	        bgcolor="bg-white">
	        <x-slot name="body">
				<p>Vuelos rutinarios con UAS </p>
				<p>Cámaras 4k</p>
				<p>Camaras Térmicas</p>
	        </x-slot>
	    </x-tarjeta-de-servicio>

	    <x-tarjeta-de-servicio 
	        titulo="Regístro"
	        icono="registro"
	        bgcolor="bg-white">
	        <x-slot name="body">
	            <p>Registro histórico</p>
	            <p>Comparativos históricos</p>
	        </x-slot>
	    </x-tarjeta-de-servicio>

	    <x-tarjeta-de-servicio 
	        titulo="Evaluación"
	        icono="evaluacion"
	        bgcolor="bg-white">
	        <x-slot name="body">
	            <p>Evaluación de riesgos</p>
	            <p>Priorización de riesgos </p>
	            <p>Definición de Detonadores</p>
	        </x-slot>
	    </x-tarjeta-de-servicio>
	</div>

	<div class="container flex flex-col md:flex-row items-stretch space-y-6 md:space-y-0 space-x-0 md:space-x-6">
	    <x-tarjeta-de-servicio 
	        titulo="Reporte"
	        icono="reporte"
	        bgcolor="bg-white">
	        <x-slot name="body">
	            <p>Reportes de alto nivel</p>
	            <p>Recomendaciones</p>
	            <p>Dashboards</p>
	        </x-slot>
	    </x-tarjeta-de-servicio>

	    <x-tarjeta-de-servicio 
	        titulo="Monitoreo"
	        icono="monitoreo"
	        bgcolor="bg-white">
	        <x-slot name="body">
	            <p>Monitoreo recurrente de riesgos</p>
	            <p>Monitoreo aereo de activos</p>
	            <p>Actualizaciones periódicas</p>
	        </x-slot>
	    </x-tarjeta-de-servicio>
	</div>
</section>

<section class="bg-white py-14 px-7">
	<div class="container">
		<img src="/img/servicios/monitoreo.png" class="w-full h-auto" alt="">
	</div>
</section>

<section class="bg-dark py-14 px-7">
	<div class="w-full md:w-9/12 mx-auto flex flex-col md:flex-row space-y-7 md:space-y-0 space-x-0 md:space-x-7">
		<div class="flex-1 bg-white p-7 shadow-md rounded-md">
			<img src="/img/servicios/skydio_2_series.jpeg" class="w-full h-auto" alt="">
			<img src="/img/servicios/skydio_2_series_description.png" class="w-full h-auto" alt="">
		</div>
		<div class="flex-1 bg-white p-7 shadow-md rounded-md">
			<img src="/img/servicios/dji_enterprise_series.jpeg" class="w-full h-auto" alt="">
			<img src="/img/servicios/dji_enterprise_series_description.png" class="w-full h-auto" alt="">
		</div>
	</div>
</section>

</x-guest-layout>
