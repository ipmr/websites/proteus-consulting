<x-guest-layout>
<x-website-titulo-de-pagina
titulo="Consultoría para tus operaciones."
subtitulo="Gracias a su versatilidad de los drones cada vez más empresas innovadoras 
implementan estos equipos para cumplir con labores que tradicionalmente eran 
costosas, peligrosas y necesitaban gran fuerza laboral y temporal. Con la 
implementación de las operaciones de drones sus necesidades pueden ser 
cubiertas con gran eficacia y rapidez, que a su vez permite el ahorro de capital 
monetario y laboral."/>
<section class="py-14 px-4">
	<div class="container flex flex-col md:flex-row items-center space-x-0 md:space-x-14 space-y-10 md:space-y-0">
		<div class="mx-auto w-full md:w-1/2">
			<h3 class="text-2xl font-bold mb-4 ">LLEVE SUS OPERACIONES AL SIGLO XXI</h3>
			<p>¿Su organización pretende implementar operaciones con aeronaves no tripuladas? Ahorre innumerables horas de trabajo y permita 
            beneficiarse de la experiencia de otros en la industria. Ayudáremos a su organización en el desarrollo de sus operaciones con el fin de 
            construir un programa para sus operaciones específicas y solventar sus necesidades.</p>
		</div>
		<div class="w-full md:w-1/2">
			<img src="{{ asset('img/servicios/consultoria.png') }}" class="shadow rounded w-full" alt="">
		</div>
	</div>
</section>

<section class="py-14 px-4 bg-oscuro">
	<div class="container">
		<div class="flex flex-col md:flex-row items-start space-x-0 md:space-x-14 space-y-10 md:space-y-0 mx-auto">
			<div class="w-full md:w-1/2">
				<h3 class="font-bold text-lg mb-7 text-white">Manual de operaciones.</h3>
				<div class="flex flex-col space-y-3 text-green-300">
					<p class="text-sm">
                    Para que el éxito de su programa de drones le ayudaremos a crear 
                    el manual de operaciones el cual le dará la autorización para realizar 
                    las operaciones especiales con drones, requeridas por la autoridad 
                    aeronáutica, creando las políticas y procedimientos necesarios para 
                    cumplir con los requisitos internos y externos
                    </p>
				</div>
			</div>
			<div class="w-full md:w-1/2">
				<h3 class="font-bold text-lg mb-7 text-white">
                Implementación, Capacitación e Integración Tecnológica. 
				</h3>
				<div class="flex flex-col space-y-3 text-green-300">
					<p class="text-sm">
                    Nuestro programa de capacitación preparará a su equipo 
                    para la certificación de pilotos base a la NOM-107-SCT3-2019, así como para el entrenamiento de vuelo para sus 
                    objetivos, integrando todo el equipo necesario para sus 
                    operaciones incluyendo: flota de aeronaves, sensores y 
                    software para su fin específico.
                    </p>
                </div>
			</div>
		</div>
	</div>
</section>
<section class="py-14 px-4 bg-oscuro">
	<div class="container">
		<div class="flex flex-col md:flex-row items-start space-x-0 md:space-x-14 space-y-10 md:space-y-0 mx-auto">

        <div class="w-full md:w-1/2">
				<h3 class="font-bold text-lg mb-7 text-white">
                Prueba de concepto. 
				</h3>
				<div class="flex flex-col space-y-3 text-green-300">
					<p class="text-sm">
                    Comienza por tener una buena comprensión de los requisitos de 
                    su negocio y qué problemas está buscando resolver. Después de 
                    un análisis detallado de la necesidad, estaremos armados con la 
                    información necesaria para mostrarle cómo los drones pueden 
                    ofrecer un valor y un retorno de la inversión en sus operaciones
                    </p>
                </div>
			</div>
            <div class="w-full md:w-1/2">
				<h3 class="font-bold text-lg mb-7 text-white">
                Soporte y servicios continuos. 
				</h3>
				<div class="flex flex-col space-y-3 text-green-300">
					<p class="text-sm">
                    Nuestro equipo dará soporte adaptado a sus 
                    necesidades para mantener sus operaciones regulares y 
                    actualizadas respecto de la industria, así como de las 
                    regulaciones
                    </p>
                </div>
			</div>			
		</div>
	</div>	<br>
		<center>	
			<a href="/contacto?int=servicio-consultoria" class="btn btn-envio">Obtener Cotización, Contactanos dando click aqui.</a>
		</center>
</div>

</section>


</x-guest-layout>
