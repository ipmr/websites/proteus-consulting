<x-guest-layout>
<x-website-titulo-de-pagina
titulo="Fotografía e inspección"
subtitulo="">
	<x-slot name="extra">
			<p class="mt-5 text-white">
				Generamos imágenes y reportes de alto valor para nuestros clientes para la continuación de sus operaciones, con el
				control visual continuo de sus equipos y activos. Asistiendo a nuestros clientes en el monitoreo de equipos, instalaciones,
				construcciones, obras en progreso. Con el fin de incrementar el control y la seguridad, manteniendo un flujo claro de
				datos, para el apoyo en la toma de desiciones y mantener la integridad de los activos de su empresa.
		    </p>
	</x-slot>
</x-website-titulo-de-pagina>

<section class="bg-cover bg-no-repeat bg-fixed py-14 px-7" style="background-image: url('/img/servicios/fotografia.jpeg')">
	<div class="container flex flex-col md:flex-row items-stretch space-y-6 md:space-y-0 space-x-0 md:space-x-6 mb-5 font-bold">
	    <x-tarjeta-de-servicio 
	        titulo="Energía"
	        bgcolor="bg-white"
	        :transparency="true">
	        <x-slot name="body">
				<p>Parques Eólios y Solares</p>
				<p>Inspección de lineas de energia</p>
				<p>Torres de transmission</p>
				<p>Lineas de gas y combustibles</p>
	        </x-slot>
	    </x-tarjeta-de-servicio>

	    <x-tarjeta-de-servicio 
	        titulo="Industria"
	        bgcolor="bg-white"
	        :transparency="true">
	        <x-slot name="body">
	            <p>Parques Industriales</p>
	            <p>Instalaciones y equipos críticos</p>
	            <p>Terreno y almacenes externos</p>
	            <p>Activos en general</p>
	            <p>Techos y areas de difícil acceso</p>
	        </x-slot>
	    </x-tarjeta-de-servicio>

	    <x-tarjeta-de-servicio 
	        titulo="Bienes raíces"
	        bgcolor="bg-white"
	        :transparency="true">
	        <x-slot name="body">
	            <p>Inspección de techos e instalaciones</p>
	            <p>Terrenos y bardas de perimetro</p>
	            <p>Mapeo</p>
	            <p>Areas rurales y de difícil acceso</p>
	        </x-slot>
	    </x-tarjeta-de-servicio>
	</div>

	<div class="container flex flex-col md:flex-row items-stretch space-y-6 md:space-y-0 space-x-0 md:space-x-6 font-bold">
	    <x-tarjeta-de-servicio 
	        titulo="Construcción e ingeniería"
	        bgcolor="bg-white"
	        :transparency="true">
	        <x-slot name="body">
	            <p>Inspección de barcos y aeronaves</p>
	            <p>Mapeo 2D</p>
	            <p>Modelado 3D</p>
	            <p>Medición de terrenos</p>
	            <p>Planeación y avance de obra</p>
	        </x-slot>
	    </x-tarjeta-de-servicio>

	    <x-tarjeta-de-servicio 
	        titulo="Minería"
	        bgcolor="bg-white"
	        :transparency="true">
	        <x-slot name="body">
	            <p>Mapeo 2D</p>
	            <p>Modelado 3D</p>
	            <p>Medición de Volúmenes</p>
	            <p>Tendencia de trabajo y medición de</p>
	            <p>eficiencia</p>
	        </x-slot>
	    </x-tarjeta-de-servicio>

	    <x-tarjeta-de-servicio 
	        titulo="Agricultura"
	        bgcolor="bg-white"
	        :transparency="true">
	        <x-slot name="body">
	            <p>Inspección de sembradíos Fumigación</p>
	            <p>Análisis espectral</p>
	            <p>Análisis térmico</p>
	            <p>Agricultura de precisión</p>
	        </x-slot>
	    </x-tarjeta-de-servicio>
	</div>
</section>

<section class="bg-white py-14 px-7">
	<div class="container mb-7">
		<div class="w-9/12 flex flex-col space-y-7 md:space-y-0 space-x-0 md:space-x-10 md:flex-row items-start  mx-auto text-center">
			<div class="flex-1 font-bold">
				<h3 class="text-xl mb-5 text-primary">NUESTRAS CAPACIDADES</h3>
				<p>Inspecciones en areas de difícil acceso</p>
				<p>Fotografía y Video 4K</p>
				<p>Fotografía y Video Térmico</p>
				<p>Mapeo 2D</p>
				<p>Modelado 3D</p>
				<p>Inspecciones internas y externas</p>
			</div>
			<div class="flex-1 font-bold">
				<h3 class="text-xl mb-5 text-primary">ENTREGABLES</h3>
				<p>Videos - HyperLapse</p>
				<p>Reportes de avance</p>
				<p>Fotografía y Video con o sin edición</p>
				<p>Dashboards</p>
				<p>Recomendaciones de alto valor</p>
				<p>Precedente histórico de cambios</p>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="w-full md:w-9/12 mx-auto">
			<img src="/img/servicios/fotografia_flechas.png" class="w-full" alt="">
		</div>
	</div>
</section>

<section class="bg-oscuro py-14 px-7">
	<div class="container">
		<div class="w-full md:w-9/12 mx-auto flex flex-col md:flex-row text-center md:text-left space-y-7 md:space-y-0 items-center md:space-x-10">
			<h2 class="font-bold text-xl md:text-5xl text-white flex-1 mb-7 md:mb-0">
				Captura tus mejores momentos con una perspectiva sin igua
			</h4>
			<div class="flex flex-1 flex-col text-lg md:text-3xl font-bold space-y-6 text-green-400">
				<h3>• Promociona tu negocio</h3>
				<h3>• Impulsa tus ventas</h3>
				<h3>• Atrae clientes</h3>
			</div>	
		</div>
	</div>
</section>

<section class="py-7 bg-white px-7">
	<div class="container">
		<div class="w-full md:w-8/12 mx-auto">
			<img src="/img/servicios/fotografia_squares.png" class="w-full" alt="">
		</div>
	</div>
</section>

<section class="py-10 bg-white px-7 border-t-2 border-gray-200">
	<div class="container">
		<div class="w-full md:w-8/12 mx-auto">
			<h3 class="text-center mb-7 text-3xl font-bold text-primary">Nuestro proceso de trabajo</h3>
			<img src="/img/servicios/fotografia_proceso.png" class="w-full" alt="">
		</div>
	</div>
</section>

</x-guest-layout>
