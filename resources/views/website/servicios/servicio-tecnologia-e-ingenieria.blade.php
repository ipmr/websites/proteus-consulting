<x-guest-layout>
<x-website-titulo-de-pagina
titulo="Diseño y mantenimiento"
subtitulo=""/>

<section class="py-14 px-4">
	<div class="container flex flex-col md:flex-row items-center space-x-0 md:space-x-14 space-y-10 md:space-y-0">
		<div class="mx-auto w-full md:w-1/2">
			<h3 class="text-2xl font-bold mb-4">Servicios de tecnología e ingeniería</h3>
			<p>En los servicios de tecnología e ingeniería implementamos todo el 
            conocimiento y las nuevas tecnologías para la solución rápida, viable, 
            tecnológica y económica de sus necesidades.
            </p>
		</div>
		<div class="w-full md:w-1/2">
			<img src="{{ asset('img/servicios/diseno_y_mantenimiento.jpeg') }}" class="shadow rounded w-full" alt="">
		</div>
	</div>
</section>

<section class="py-14 px-4 bg-oscuro">
	<div class="container">
		<h3 class="text-lg font-bold text-white mb-7">
        Entre los servicios de ingeniería aérea que brindamos se encuentran los siguientes:		</h3>
		<div class="flex flex-col md:flex-row items-start space-x-0 md:space-x-10 space-y-10 md:space-y-0">
			<div class="w-full md:w-1/2">
				<div class="flex flex-col space-y-3">
					<p class="text-white">Mantenimiento preventivo y correctivo</p>
					<p class="text-white">Actualización de Software y Hardware</p>
                    <p class="text-white">Diseño, modelado y prueba de partes impresas 3D</p>
                    <p class="text-white">Armado de Aeronaves no tripuladas</p>
                    <p class="text-white">Mejora de diseño para aeronaves</p>
				</div>
			</div>
			<div class="w-full md:w-1/2">
				<div class="flex flex-col space-y-3">
                    <p class="text-white">Proyectos de investigación</p>
                    <p class="text-white">Comprobación de funcionamiento de equipo aéreo</p>
                    <p class="text-white">Desarrollo de concepto</p>
                    <p class="text-white">Pruebas de concepto</p>
                    <p class="text-white">Mejoras de diseño</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="py-14 px-4">
	<div class="container flex flex-col md:flex-row items-center space-x-0 md:space-x-14 space-y-10 md:space-y-0">
		<div class="w-full md:w-1/2">
        <img src="{{ asset('img/servicios/tecnologia_e_ingenieria_1.jpg') }}" class="shadow rounded w-full" alt="">
		</div>
        <div class="mx-auto w-full md:w-1/2">
			<h3 class="text-2xl font-bold mb-4">Mantenimiento preventivo y correctivo de RPAS</h3>
			<p>Nos encargamos de realizar el mantenimiento preventivo y correctivo 
            de una gran variedad de drones del mercado para extender la vida 
            útil y hacer de su equipo una aeronave confiable y eficiente,</p>
		</div>
	</div>
</section>

<section class="py-14 px-4 bg-green-100">
	<div class="container">
		<div class="flex flex-col md:flex-row items-start space-y-10 md:space-y-0">
			<div class="w-full md:w-1/2">
				<h3 class="font-bold mb-7">El mantenimiento preventivo incluye lo siguiente:</h3>
                <p>
                Realizar el mantenimiento preventivo a tu drone puede alargar su vida y mejorar su desempeño
                </p>
                <br>
				<div class="flex flex-col space-y-3 font-sm">
					<p>1. Verificación de Software
                    </p> 
                    <p>
                    2. Verificación de Hardware (Revisión de todos los elementos)
                    </p>
                    <p>
                    3. Verificación y actualización de firmware
                    </p>
                    <p>
                    5. Comprobación de funciones operativas
                    </p>
                    <p>
                    7. Inspección estructural
                    </p>
                    <p>
                    8. Inspección de baterías
                    </p>
                    <p>
                    9. Limpiezas
                    </p>
				</div>
			</div>
			<div class="w-full md:w-1/2">
				<h3 class="font-bold mb-7">El mantenimiento correctivo incluye lo siguiente:</h3>
                <p>Si tu drone sufrió un percance nosotros lo reparamos</p>
                <br>
				<div class="flex flex-col space-y-3 font-sm">
					<p>
                    1. Búsqueda del problema
                    </p>
                    <p>
                    2. Análisis del problema
                    </p>
                    <p>
                    3. Corrección del problema
                    </p>
                    <p>
                    4. Comprobación de operatividad
                    </p>
                    <p>
                    5. Limpieza
                    </p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="py-14 px-4">
	<div class="container flex flex-col md:flex-row items-center space-x-0 md:space-x-14 space-y-10 md:space-y-0">
		<div class="mx-auto w-full md:w-1/2">
			<h3 class="text-2xl font-bold mb-4">Diseño y manufactura de partes con Impresión 3D</h3>
			<p>Producimos estructuras complejas y ligeras para las 
            aeronaves no tripuladas gracias a la impresión 3D y 
            los distintos materiales que utilizamos. Reduciendo 
            costos de materiales y tiempos de producción durante el proceso de prototipo. 
            </p>
		</div>
		<div class="w-full md:w-1/2">
			<img src="{{ asset('img/servicios/tecnologia_e_ingenieria_3.png') }}" class="shadow rounded w-full" alt="">
		</div>
	</div>
</section>

<section class="py-14 px-4 bg-oscuro">
	<div class="container">
		<div class="flex flex-col md:flex-row items-start space-x-0 md:space-x-14 space-y-10 md:space-y-0 mx-auto">
			<div class="w-full md:w-1/2">
				<h3 class="font-bold text-lg mb-7 text-white">Contamos con la impresora 3D Ender 3 Pro Creality de cama 
                caliente con las siguientes especificaciones:</h3>
				<div class="flex flex-col space-y-3 text-green-300">
					<p class="text-sm">
                        1. Tamaño de impresión: 220*220*250mm 
					</p>
					<p class="text-sm">
                        2. Precisión de impresión: ± 0.1mm
					</p>
					<p class="text-sm">
						3.Temperatura de cama: ≤100oC
					</p>
					<p class="text-sm">
						4.Diámetro del extrusor: 0.4mm
					</p>
					<p class="text-sm">
						5.Filamento: 1.75mm PLA,ABS,TPU
					</p>
				</div>
			</div>
			<div class="w-full md:w-1/2">
				<p class="mb-7 text-lg font-bold text-white">
                Armado de Aeronaves no tripuladas
				</p>
				<p class="text-white">
                ¿Tienes en mente crear un drone acorde a tus necesidades y no sabes por dónde comenzar?
 				</p>
                 <br>
                 <p class="text-white">
                 En Proteus Consulting UAS te ayudamos y asesoramos para que puedas cumplir tus objetivos 
                 de armado de tu drone para la misión especifica para la que lo necesitas, contando con gran 
                 experiencia en el diseño y operaciones de aeronaves no tripuladas.
                 </p>
			</div>
		</div>
	</div>
</section>

<section class="py-14 px-4">
	<div class="container">
		<h3 class="text-lg font-bold mb-7">
        Los puntos que cubrimos con este servicio son:
		</h3>
		<div class="flex flex-col md:flex-row items-start space-x-0 md:space-x-10 space-y-10 md:space-y-0">
			<div class="w-full md:w-1/2">
				<div class="flex flex-col space-y-3">
					<p>
                    1. Asesoría
					</p>
					<p>
                    2. Recomendación de compra
					</p>
					<p>
                    3. Búsqueda y compra de componentes
					</p>
					<p>
                    4. Armado total de Drone
					</p>
					<p>
                    5. Armado parcial de Drone
					</p>
					<p>
                    6. Reconstrucción de Drone
					</p>
					<p>
                    7. Cambio de componentes
					</p>
                    <p>
                    8. Aplicación de telemetría
                    </p>
				</div>
			</div>
			<div class="w-full md:w-1/2">
		    	<img src="{{ asset('img/servicios/tecnologia_e_ingenieria_2.jpg') }}" class="shadow rounded w-full" alt="">
			</div>
		</div>
	</div>
		<br>
		<center>	
			<a href="/contacto?int=servicio-tecnologia-e-ingenieria" class="btn btn-envio">Obtener Cotización, Contactanos dando click aqui.</a>
		</center>
		<br>
</section>

</x-guest-layout>
