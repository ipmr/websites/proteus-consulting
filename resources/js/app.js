require('./bootstrap');
require('alpinejs');
import Vue from 'vue';
import {WOW} from 'wowjs';
import CKEditor from '@ckeditor/ckeditor5-vue2';
Vue.use( CKEditor );
import BannersDePortada from './componentes/BannersDePortada.vue';
import MobileNavBtn from './componentes/MobileNavBtn.vue';
import WebsiteNavLinks from './componentes/WebsiteNavLinks.vue';
import FormularioPagina from './admin/forms/FormularioPagina.vue';
const app = new Vue({
	components: {
		BannersDePortada,
		MobileNavBtn,
		WebsiteNavLinks,
		FormularioPagina
	},
	mounted(){
		this.iniciar_wow_js();
	},
	methods: {
		iniciar_wow_js(){
			let t = this;
			var wow = new WOW(
				{
					boxClass:     'wow',      // animated element css class (default is wow)
					animateClass: 'animate__animated', // animation css class (default is animated)
					offset:       0,          // distance to the element when triggering the animation (default is 0)
					mobile:       false,       // trigger animations on mobile devices (default is true)
					live:         false,       // act on asynchronously loaded content (default is true)
				}
			);
			wow.init();
		}
	}
}).$mount('#app');
