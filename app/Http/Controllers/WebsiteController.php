<?php
namespace App\Http\Controllers;
use App\Mail\MailDeBienvenida;
use App\Mail\FormularioDeContacto;
use App\Mail\NuevoMensajeDeContacto;
use App\Mail\MailNuevoRegistro;
use App\Models\Registro;
use Illuminate\Http\Request;
use Mail;
use Validator;
class WebsiteController extends Controller
{
    public function index()
    {
        return view('website.index');
    }
    public function nosotros(){
        return view('website.nosotros');
    }
    public function curso($curso){
        return view('website.cursos.' . $curso);
    }
    public function servicio($servicio){
        return view('website.servicios.' . $servicio);
    }
    public function contacto()
    {
        return view('website.contacto');
    }
    public function registro(Request $request)
    {
        $validar  = $this->validar_datos_de_formulario($request);
        $registro = $this->guardar_nuevo_registro($request);
        $emails   = $this->enviar_emails_de_registro($registro);
        return redirect()
            ->back()
            ->with([
                'registro' => $registro,
                'mensaje_de_registro' => 'Gracias por registrarte en Proteus UAS.'
            ]);
    }
    public function nuevo_contacto(Request $request)
    {
        $validar = $this->validar_datos_de_contacto_form($request);
        $enviar  = $this->enviar_email_de_contacto($request);
        $notificar = $this->nuevo_mensaje_de_contacto($request);
        return redirect()
        ->back()
        ->with([
            'mensaje' =>$enviar,
            'mesaje_de_contacto' => 'Gracias por contactarnos, nos comunicaremos con usted a la brevedad posible, Proteus UAS'
        ]);

    }

    public function enviar_emails_de_registro($registro)
    {
        $this->darle_bienvenida_al_nuevo_registro($registro);
        $this->notificar_al_administrador_sobre_nuevo_registro($registro);
    }
    
    public function darle_bienvenida_al_nuevo_registro($registro)
    {
        Mail::to($registro->email)->send(new MailDeBienvenida($registro));
    }
    
    public function notificar_al_administrador_sobre_nuevo_registro($registro){
       
        Mail::to(env('MAIL_FROM_ADDRESS'))->send(new MailNuevoRegistro($registro));
    }
    
    public function guardar_nuevo_registro($request)
    {
        $registro           = new Registro();
        $registro->nombre   = $request->nombre;
        $registro->email    = $request->email;
        $registro->compania = $request->compania;
        $registro->save();
        return $registro;
    }
    
    public function validar_datos_de_formulario($request)
    {
        $reglas_de_validacion = [
            'nombre'   => 'required|string',
            'email'    => 'required|email|unique:registros,email',
            'compania' => 'nullable|string',
        ];
        $mensajes_de_validacion = [
            'nombre.required' => 'Por favor introduce tu nombre completo.',
            'nombre.string'   => 'El nombre proporcionado no es válido.',
            'email.required'  => 'Por favor introduce tu correo electrónico.',
            'email.email'     => 'El correo electrónico proporcionado no es válido.',
            'email.unique'    => 'El correo electrónico proporcionado ya se encuentra registrado.',
            'compania.string' => 'El nombre de la compañía no es válido',
        ];
        $validar = Validator::make($request->all(), $reglas_de_validacion, $mensajes_de_validacion)->validate();
    }
    
    public function enviar_email_de_contacto($request)
    {
        $datos = [

            'nombre' => $request->nombre,
            'email'  => $request->email,
            'asunto' => $request->asunto,
            'mensaje'=> $request->mensaje,

        ];
        Mail::to($request->email)->send(new FormularioDeContacto($datos));
    }
    
    public function nuevo_mensaje_de_contacto($request)
    {
        $datos = [
            'nombre' => $request->nombre,
            'email'  => $request->email,
            'asunto' => $request->asunto,
            'mensaje'=> $request->mensaje,
            'interes'=> $request->interes,
        ];
        Mail::to(env('MAIL_FROM_ADDRESS'))->send(new NuevoMensajeDeContacto($datos));
    }
    
    public function validar_datos_de_contacto_form($request)
    {
        $reglas_de_validacion = [
            'nombre'   =>  'required|string',
            'email'    =>  'required|email',
            'asunto'   =>  'required|string',
            'mensaje'  =>  'required|string',
        ];
        $mensajes_de_validacion = [
            'nombre.required' => 'Por favor introduce tu nombre completo.',
            'nombre.string'   => 'El nombre proporcionado no es valido.',
            'email.required'  => 'por facor introduce tu correo electronico.',
            'email.email'     => 'El correo electrónico proporcionado no es válido.',
            'asunto.required' => 'Por favor introduce el asunto deseado.',
            'asunto.string'   => 'El asunto proporcionado no es valido.',
            'mensaje.required'=> 'Por favor introduce el mensaje deseado.',
            'mensaje.string'  => 'El mensaje introducido no es valido.',
        ];
        $validar = Validator::make($request->all(), $reglas_de_validacion, $mensajes_de_validacion)->validate();
    }
}