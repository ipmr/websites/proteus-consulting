<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailDeBienvenida extends Mailable
{
    use Queueable, SerializesModels;
    public $nombre;
    public $email;
    public $compania;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->nombre   = $request['nombre'];
        $this->email    = $request['email'];
        $this->compania = $request['compania'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Bienvenido a Proteus UAS')
            ->markdown('mails.mail_de_bienvenida')
            ->with([
                'nombre'   => $this->nombre,
                'email'    => $this->email,
                'compania' => $this->compania,
            ]);
    }
}
