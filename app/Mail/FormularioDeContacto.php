<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FormularioDeContacto extends Mailable
{
    use Queueable, SerializesModels;

    public $nombre;
    public $asunto;
    public $email;
    public $mensaje;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($datos)
    {
        $this->nombre = $datos['nombre'];
        $this->asunto = $datos['asunto'];
        $this->email = $datos['email'];
        $this->mensaje = $datos['mensaje'];

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Gracias por contactarnos, te responderemos a la brevedad')      
            ->markdown('mails.formulario_de_contacto')
            ->with([
                'nombre'   => $this->nombre,
                'asunto'   => $this->asunto,
                'email'    => $this->email,
                'mensaje'  => $this->mensaje,
            ]);
    }
}
