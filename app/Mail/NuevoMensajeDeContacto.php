<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NuevoMensajeDeContacto extends Mailable
{
    use Queueable, SerializesModels;

    public $nombre;
    public $asunto;
    public $email;
    public $mensaje;
    public $interes;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($datos)
    {
        $this->nombre = $datos['nombre'];
        $this->asunto = $datos['asunto'];
        $this->email = $datos['email'];
        $this->mensaje = $datos['mensaje'];
        $this->interes = $datos['interes'];
        }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Nuevo mensaje recibido')      
            ->markdown('mails.nuevo_mensaje_de_contacto')
            ->with([
                'nombre'   => $this->nombre,
                'asunto'   => $this->asunto,
                'email'    => $this->email,
                'mensaje'  => $this->mensaje,
            ]);    }
}
