<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailNuevoRegistro extends Mailable
{
    use Queueable, SerializesModels;

    public $nombre;
    public $email;
    public $compania;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->nombre   = $request['nombre'];
        $this->email    = $request['email'];
        $this->compania = $request['compania'];    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('¡Notificación! Nuevo usuario registrado')
            ->markdown('mails.mail_nuevo_registro')
            ->with([
                'nombre'   => $this->nombre,
                'email'    => $this->email,
                'compania' => $this->compania,
            ]);    }
}
