<?php

namespace App\View\Components;

use Illuminate\View\Component;

class TarjetaDeServicio extends Component
{


    public $titulo;
    public $icono;
    public $bgcolor;
    public $transparency;
    public $ruta;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($titulo = null, $icono = null, $bgcolor = null, $ruta = null, $transparency = false)
    {
        $this->transparency = $transparency;
        $this->titulo = $titulo;
        $this->icono = $icono;
        $this->bgcolor = $bgcolor ? $bgcolor : 'bg-green-100';
        $this->ruta = $ruta;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.tarjeta-de-servicio');
    }
}
