<?php

namespace App\View\Components;

use Illuminate\View\Component;

class FormularioDeRegistro extends Component
{

    public $clases;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($clases)
    {
        $this->clases = $clases;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.formulario-de-registro');
    }
}
