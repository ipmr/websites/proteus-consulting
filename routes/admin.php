<?php
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){
	Route::get('/', [AdminController::class, 'index'])->name('admin');
	Route::get('/paginas', [AdminController::class, 'paginas'])->name('paginas');
	Route::get('/paginas/nueva', [AdminController::class, 'nueva_pagina'])->name('nueva-pagina');
	Route::get('/imagenes', [AdminController::class, 'imagenes'])->name('imagenes');
});