<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebsiteController;
Route::get('/', [WebsiteController::class, 'index'])->name('inicio');
Route::get('/nosotros', [WebsiteController::class, 'nosotros'])->name('nosotros');
Route::get('/cursos/{curso}', [WebsiteController::class, 'curso'])->name('curso');
Route::get('/servicios/{servicio}', [WebsiteController::class, 'servicio'])->name('servicio');
Route::get('/contacto', [WebsiteController::class, 'contacto'])->name('contacto');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
Route::post('registro-de-usuario', [WebsiteController::class, 'registro'])->name('registro-de-usuario');
Route::post('nuevo-contacto', [WebsiteController::class, 'nuevo_contacto'])->name('nuevo-contacto');
