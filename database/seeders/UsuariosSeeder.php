<?php
namespace Database\Seeders;
use App\Models\User;
use Illuminate\Database\Seeder;
class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create([
        	'name' => 'Jonathan Velazquez',
        	'email' => 'admin@proteusuas.com'
        ]);
    }
}